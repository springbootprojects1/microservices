package com.kasi.micro.services.account;

import com.kasi.micro.accounts.AccountRepository;
import com.kasi.micro.accounts.AccountsConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(AccountsConfiguration.class)
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    public static void main(String[] args) {
        // Will configure using accounts-service.yml
        System.setProperty("spring.config.name", "account-service");

        SpringApplication.run(AccountService.class, args);
    }
}