package com.kasi.micro.services.client;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Client controller, fetches Account info from the microservice via
 * {@link WebAccountsService}.
 *
 * @author Paul Chapman
 */
@Controller
public class WebAccountsController {
    @Autowired
	protected WebAccountsService accountsService;

	protected Logger logger = Logger.getLogger(WebAccountsController.class
			.getName());

	public WebAccountsController(WebAccountsService accountsService) {
		this.accountsService = accountsService;
	}
}